# 1 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c"
# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"








































































	

 


















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 263 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 502 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 505 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 528 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 562 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 585 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 609 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);
int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);


 
 
 
 
 
 
# 676 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											int * col_name_len);
# 737 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);


 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   int const in_len,
                                   char * * const out_str,
                                   int * const out_len);
# 762 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 774 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 782 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 788 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );

int lr_save_searched_string(char *buffer, long buf_size, unsigned int occurrence,
			    char *search_string, int offset, unsigned int param_val_len, 
			    char *param_name);

 
char *   lr_string (char * str);

 
# 859 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 866 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 888 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 964 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 993 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


# 1005 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char *sourceString, char *fromEncoding, char *toEncoding, char *paramName);





 
 

















# 1 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c" 2

# 1 "globals.h" 1



 
 

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 1
 







# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h" 1
 






















































 




 








 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 










# 596 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"


# 609 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



























# 647 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 715 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



 
 
 






# 10 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 2












 






 











  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 





 
 
 


# 7 "globals.h" 2

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrw_custom_body.h" 1
 





# 8 "globals.h" 2


 
 


 

random_Generator(char* paramname, int length) {
 char buffer[32] = "";
 int r,i;
 char c;
 srand((unsigned int)_time32(0));
 for (i = 0; i < length; i++) {
 r = rand() % 25 + 65;
 c = (char)r;
 buffer[i] = c;
  if (buffer[i] == buffer[i-1])
  {
  r = rand() % 25 + 65;
  c = (char)r;
  buffer[i] = c;
  }
 }
 lr_save_string(buffer, paramname);
return 0;
}




# 2 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c" 2

# 1 "vuser_init.c" 1
vuser_init()
{

	random_Generator( "RandomFullName", 6);

	random_Generator( "RandomQuestionText", 20);

	web_save_timestamp_param("timeStamp", "LAST");


	return 0;
}
# 3 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c" 2

# 1 "Action.c" 1

 
int i;


Action()
{

	web_set_sockets_option("SSL_VERSION", "TLS");


     

	lr_start_transaction("goToHomePage");
	
	web_url("www.askorty.com", 
			"URL=https://www.askorty.com/", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");
	
	lr_end_transaction("goToHomePage", 2);
	
	
	lr_start_transaction("scrollToDown");
	
	web_url("www.askorty.com_2", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=2", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			"LAST");

	web_url("www.askorty.com_3", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=3", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			"LAST");
	
	web_url("www.askorty.com_4", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=4", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			"LAST");
	
	web_url("www.askorty.com_5", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=5", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			"LAST");
	
	lr_end_transaction("scrollToDown", 2);


	lr_start_transaction("sortItemOnHomePageByRecentFirst");

	web_url("www.askorty.com_6", 
			"URL=https://www.askorty.com/?sort_by=time", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			"LAST");

	lr_end_transaction("sortItemOnHomePageByRecentFirst", 2);

	 


     

	lr_start_transaction("goToListUserPage");


	 
	web_reg_save_param("listUrlCelebrityDetailPages", "LB=<div class='card'>\n<a href='/", "RB='>", "ORD=ALL", "LAST");

	web_url("users", 
			"URL=https://www.askorty.com/users", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");

	lr_end_transaction("goToListUserPage", 2);


	lr_start_transaction("searchAction");

	web_url("auto_suggest", 
			"URL=https://www.askorty.com/users/auto_suggest?search_by=steven", 
			"Resource=0", 
			"Referer=https://www.askorty.com/users", 
			"Mode=HTML", 
			"LAST");

	lr_end_transaction("searchAction", 2);

     


	 

	lr_start_transaction("goToCelebrityDetailsPage");


	 
	lr_save_string(lr_paramarr_random("listUrlCelebrityDetailPages"),"celebrityDetailPage");

	web_url("celebrity_detail_page", 
			"URL=https://www.askorty.com/{celebrityDetailPage}", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");

	lr_end_transaction("goToCelebrityDetailsPage", 2);

	 


	 

	lr_start_transaction("goToSignUpPage");

	web_reg_save_param("authenticityToken", "LB=authenticity_token\" value=\"", "RB=\"", "LAST");

	web_url("signup", 
			"URL=https://www.askorty.com/users/signup", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");

    lr_end_transaction("goToSignUpPage", 2);


	lr_start_transaction("setUserRegDataAndSignUp");

    web_submit_data("signup_2", 
					"Action=https://www.askorty.com/users/signup", 
					"Method=POST", 
					"RecContentType=text/html", 
					"Referer=https://www.askorty.com/users/signup", 
					"Mode=HTML", 
					"EncodeAtSign=YES", 
					"ITEMDATA", 
					"Name=utf8", "Value=✓", "ENDITEM", 
					"Name=authenticity_token", "Value={authenticityToken}", "ENDITEM", 
					"Name=user[name]", "Value=FullUName{timeStamp}", "ENDITEM", 
					"Name=user[username]", "Value=us{timeStamp}", "ENDITEM", 
					"Name=user[email]", "Value=test_user_{timeStamp}@test.com", "ENDITEM", 
					"Name=user[password]", "Value={timeStamp}", "ENDITEM", 
                    "LAST");

	 

	lr_end_transaction("setUserRegDataAndSignUp", 2);

	
	lr_start_transaction("activateNewAccount");


	 
    web_reg_save_param("confirmLink", "LB=https://", "RB=", "ORD=1", "LAST");

	web_url("goToClinkPage", 
			"URL=https://www.askorty.com/us{timeStamp}/clink", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");
	
	web_url("verifyAccount", 
			"URL=https://{confirmLink}", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			"LAST");


	lr_end_transaction("activateNewAccount", 2);


	lr_start_transaction("goToLoginPage");

	web_reg_save_param("csrfToken", "LB=csrf-token\" content=\"", "RB=\" />\n<link rel=\"stylesheet\"", "LAST");

	web_url("login", 
			"URL=https://www.askorty.com/users/login", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=",
			"Mode=HTML", 
			"LAST");

	lr_end_transaction("goToLoginPage", 2);

	 


	 

	lr_start_transaction("setAuthDataAndSignIn");	

	web_add_header ("X-CSRF-Token", "{csrfToken}");

	web_submit_data("login_2", 
					"Action=https://www.askorty.com/users/login", 
					"Method=POST", 
					"RecContentType=application/json", 
					"Referer=https://www.askorty.com/users/login",  
					"Mode=HTML", 
					"EncodeAtSign=YES", 
					"ITEMDATA", 
					"Name=utf8", "Value=✓", "ENDITEM", 
					"Name=user[login]", "Value=test_user_{timeStamp}@test.com", "ENDITEM", 
					"Name=user[password]", "Value={timeStamp}", "ENDITEM", 
					"Name=user[remember_me]", "Value=0", "ENDITEM", 
					"LAST");

    lr_end_transaction("setAuthDataAndSignIn", 2);


	lr_start_transaction("goToHomePageAfterAuthorization");

	web_url("www.askorty.com", 
			"URL=https://www.askorty.com/", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/users/login", 
			"Mode=HTML", 
            "LAST");

    lr_end_transaction("goToHomePageAfterAuthorization", 2);

	 


	 


	lr_start_transaction("goToQuestionPage");

	web_reg_save_param("listIdQuestion", "LB=data-id='", "RB= data-type", "ORD=ALL", "LAST");
	 
	web_url("question", 
			"URL=https://www.askorty.com/?type=question", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=",
			"Mode=HTML", 
			"LAST");
	
	lr_end_transaction("goToQuestionPage", 2);
	
	
	i = 0;
	
	do {
	
		lr_start_transaction("goToEmbedCardWithoutAnswer");
	
    	lr_save_string(lr_paramarr_random("listIdQuestion"),"idQuestion");
	
    	 
	
		web_url("embedCardWithoutAnswer", 
				"URL=https://www.askorty.com/ecard/{idQuestion}", 
				"Resource=1", 
				"RecContentType=text/javascript", 
				"Referer=", 
				"LAST");
	
		
		lr_end_transaction("goToEmbedCardWithoutAnswer", 2);
	
	
		i = i + 1;
	
	} while ( i < 2 );


	 


	 

	lr_start_transaction("prepareGoToEmbedCardWithAnswerPage");
	
		
	lr_start_transaction("goToAnswersPageForGetAllEmbedCardWithUrlAnswer");
	
	
	web_reg_save_param("listIdAnswer", "LB=ajax-modal-opener' data-href='/questions/", "RB=/embed_code", "ORD=ALL", "LAST");
		
	web_url("answerpage", 
			"URL=https://www.askorty.com/?type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Snapshot=t1.inf", 
			"Mode=HTML", 
			"LAST");
	
	web_url("answerpage", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/?type=answer", 
			"Snapshot=t2.inf", 
			"Mode=HTML", 
			"LAST");
	
	web_url("www.askorty.com_3", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/?type=answer", 
			"Snapshot=t3.inf", 
			"Mode=HTML", 
			"LAST");
	
		
	lr_end_transaction("goToAnswersPageForGetAllEmbedCardWithUrlAnswer", 2);
	
		
	i = 0;

	do {
	
		lr_start_transaction("goToEmbedCardWithAnswerPage");
	
    	lr_save_string(lr_paramarr_random("listIdAnswer"),"embedCode");
		 

		web_url("EmbedCardWithAnswer", 
				"URL=https://www.askorty.com/ecard/{embedCode}", 
				"Resource=1", 
				"RecContentType=text/javascript", 
				"Referer=", 
				"LAST");
	
        lr_end_transaction("goToEmbedCardWithAnswerPage", 2);
	
    	i = i +1;
	
	}while ( i < 2);
	
		
	lr_end_transaction("prepareGoToEmbedCardWithAnswerPage", 2);

	 


	 

	i = 0;

    do {
	

		lr_start_transaction("AskQuestionToCelebrity");
	
		lr_save_string(lr_paramarr_random("listUrlCelebrityDetailPages"),"celebrityDetailPage");
	
		web_url("CelebrityQuestionPage", 
				"URL=https://www.askorty.com/{celebrityDetailPage}?type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=", 
				"Mode=HTML", 
				"LAST");
	
		web_reg_save_param("askByUserID", "LB=value=\"", "RB=\" type=\"hidden\" name=\"question[asked_by_user]\"", "LAST");
		web_reg_save_param("askedToID", "LB=value=\"", "RB=\" name=\"question[asked_to][]\"", "LAST");
	
		web_url("openWindowForQuestion", 
				"URL=https://www.askorty.com/{celebrityDetailPage}/questions/new?modal=true&msg=Success!%20You%20logged%20in.%20Please%20close%20this%20popup%20and%20click%20the%20'Ask%20me%20a%20question'%20button%20again", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/{celebrityDetailPage}", 
				"Mode=HTML", 
				"LAST");
	
		 
		 
	
	
		web_submit_data("askQuestions", 
						"Action=https://www.askorty.com/{celebrityDetailPage}/questions", 
						"Method=POST", 
						"RecContentType=application/json", 
						"Referer=https://www.askorty.com/{celebrityDetailPage}?type=question", 
						"Mode=HTML", 
						"ITEMDATA", 
						"Name=utf8", "Value=✓", "ENDITEM", 
						"Name=question[asked_by_user]", "Value={askByUserID}", "ENDITEM", 
						"Name=question[asked_to][]", "Value={askedToID}", "ENDITEM", 
						"Name=question[text]", "Value=test_question {RandomQuestionText}?", "ENDITEM", 
						"LAST");
	
		lr_end_transaction("AskQuestionToCelebrity", 2);

		i = i + 1;

	}while(i < 4);

	 


	 
        
	i = 0;

    do {

		lr_start_transaction("postCommentToAnswer");

		web_reg_save_param("listAnswersOfCelebritys", "LB=data-href='/questions/", "RB=/comments?modal=true'", "ORD=ALL", "LAST");

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
                "Mode=HTML", 
				"LAST");

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				"LAST");


		lr_save_string(lr_paramarr_random("listAnswersOfCelebritys"),"idAnswer");

		 


		web_url("comments", 
				"URL=https://www.askorty.com/questions/{idAnswer}/comments?modal=true", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				"LAST");
	 
		web_submit_data("comments_2", 
						"Action=https://www.askorty.com/questions/{idAnswer}/comments", 
						"Method=POST", 
						"RecContentType=application/json", 
						"Referer=https://www.askorty.com/?type=answer", 
						"Mode=HTML", 
						"ITEMDATA", 
						"Name=text", "Value=TestComment {timeStamp}", "ENDITEM", 
						"LAST");
	 
		lr_end_transaction("postCommentToAnswer", 2);

		i = i + 1;

	}while(i < 4);

	 


	 

	i = 0;

	do {
		
		lr_start_transaction("voteForQuestion");

		web_reg_save_param("listOfQuestionsToCelebrities", "LB=data-id='", "RB=' data-loading-text='Voting", "ORD=ALL", "LAST");

		web_url("www.askorty.com", 
				"URL=https://www.askorty.com/?type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=", 
				"Mode=HTML", 
				"LAST");

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=question", 
				"Mode=HTML", 
				"LAST");

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=question", 
				"Mode=HTML", 
				"LAST");

		lr_save_string(lr_paramarr_random("listOfQuestionsToCelebrities"),"idQuestion");

		 

		web_custom_request("utility", 
						   "URL=https://www.askorty.com/questions/{idQuestion}/utility?type=add_request", 
						   "Method=POST", 
						   "Resource=0", 
						   "RecContentType=application/json", 
						   "Referer=https://www.askorty.com/?type=question", 
						   "Mode=HTML", 
						   "EncType=", 
						   "LAST");


		lr_end_transaction("voteForQuestion", 2);

		i = i + 1;

	} while ( i < 2);

	 


	 

	i = 0;

	do {

		lr_start_transaction("likeAnAnswer");

		web_reg_save_param("listAnswersOfCelebritys", "LB=data-href='/questions/", "RB=/comments?modal=true'", "ORD=ALL", "LAST");

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Snapshot=t2.inf", 
				"Mode=HTML", 
				"LAST");

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				"LAST");


		lr_save_string(lr_paramarr_random("listAnswersOfCelebritys"),"idAnswer");

		 


		web_custom_request("utility", 
						   "URL=https://www.askorty.com/questions/{idAnswer}/utility?type=like", 
						   "Method=POST", 
						   "Resource=0", 
						   "RecContentType=application/json", 
						   "Referer=https://www.askorty.com/?type=answer", 
						   "Mode=HTML", 
						   "EncType=", 
						   "LAST");
	 
		lr_end_transaction("likeAnAnswer", 2);

		i = i + 1;

	} while ( i < 2 );

	 


	return 0;
}
# 4 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 5 "e:\\testmatick\\project\\askorty.com\\actions\\askortyactions\\\\combined_AskortyActions.c" 2

