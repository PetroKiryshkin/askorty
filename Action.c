
//counter for loop
int i;


Action()
{

	web_set_sockets_option("SSL_VERSION", "TLS");


    /*********************** Homepage with scroll and sorting ***********************/

	lr_start_transaction("goToHomePage");
	
	web_url("www.askorty.com", 
			"URL=https://www.askorty.com/", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);
	
	lr_end_transaction("goToHomePage", LR_AUTO);
	
	
	lr_start_transaction("scrollToDown");
	
	web_url("www.askorty.com_2", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=2", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			LAST);

	web_url("www.askorty.com_3", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=3", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			LAST);
	
	web_url("www.askorty.com_4", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=4", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			LAST);
	
	web_url("www.askorty.com_5", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=5", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			LAST);
	
	lr_end_transaction("scrollToDown", LR_AUTO);


	lr_start_transaction("sortItemOnHomePageByRecentFirst");

	web_url("www.askorty.com_6", 
			"URL=https://www.askorty.com/?sort_by=time", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/", 
			"Mode=HTML", 
			LAST);

	lr_end_transaction("sortItemOnHomePageByRecentFirst", LR_AUTO);

	/************************************************************************************/


    /**************************** List page and search in it ****************************/

	lr_start_transaction("goToListUserPage");


	//  Get all url of celebrities
	web_reg_save_param("listUrlCelebrityDetailPages", "LB=<div class='card'>\n<a href='/", "RB='>", "ORD=ALL", LAST);

	web_url("users", 
			"URL=https://www.askorty.com/users", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);

	lr_end_transaction("goToListUserPage", LR_AUTO);


	lr_start_transaction("searchAction");

	web_url("auto_suggest", 
			"URL=https://www.askorty.com/users/auto_suggest?search_by=steven", 
			"Resource=0", 
			"Referer=https://www.askorty.com/users", 
			"Mode=HTML", 
			LAST);

	lr_end_transaction("searchAction", LR_AUTO);

    /************************************************************************************/


	/******************************* Celebrity detail page ******************************/

	lr_start_transaction("goToCelebrityDetailsPage");


	// Get URL celebrity 
	lr_save_string(lr_paramarr_random("listUrlCelebrityDetailPages"),"celebrityDetailPage");

	web_url("celebrity_detail_page", 
			"URL=https://www.askorty.com/{celebrityDetailPage}", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);

	lr_end_transaction("goToCelebrityDetailsPage", LR_AUTO);

	/************************************************************************************/


	/************************************* Sing up **************************************/

	lr_start_transaction("goToSignUpPage");

	web_reg_save_param("authenticityToken", "LB=authenticity_token\" value=\"", "RB=\"", LAST);

	web_url("signup", 
			"URL=https://www.askorty.com/users/signup", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);

    lr_end_transaction("goToSignUpPage", LR_AUTO);


	lr_start_transaction("setUserRegDataAndSignUp");

    web_submit_data("signup_2", 
					"Action=https://www.askorty.com/users/signup", 
					"Method=POST", 
					"RecContentType=text/html", 
					"Referer=https://www.askorty.com/users/signup", 
					"Mode=HTML", 
					"EncodeAtSign=YES", 
					ITEMDATA, 
					"Name=utf8", "Value=✓", ENDITEM, 
					"Name=authenticity_token", "Value={authenticityToken}", ENDITEM, 
					"Name=user[name]", "Value=FullUName{timeStamp}", ENDITEM, 
					"Name=user[username]", "Value=us{timeStamp}", ENDITEM, 
					"Name=user[email]", "Value=test_user_{timeStamp}@test.com", ENDITEM, 
					"Name=user[password]", "Value={timeStamp}", ENDITEM, 
                    LAST);

	//lr_output_message("EMAIL test_user_%s@test.com",lr_eval_string("{timeStamp}"));

	lr_end_transaction("setUserRegDataAndSignUp", LR_AUTO);

	
	lr_start_transaction("activateNewAccount");


	// Get confirm link for activate new account
    web_reg_save_param("confirmLink", "LB=https://", "RB=", "ORD=1", LAST);

	web_url("goToClinkPage", 
			"URL=https://www.askorty.com/us{timeStamp}/clink", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);
	
	web_url("verifyAccount", 
			"URL=https://{confirmLink}", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Mode=HTML", 
			LAST);


	lr_end_transaction("activateNewAccount", LR_AUTO);


	lr_start_transaction("goToLoginPage");

	web_reg_save_param("csrfToken", "LB=csrf-token\" content=\"", "RB=\" />\n<link rel=\"stylesheet\"", LAST);

	web_url("login", 
			"URL=https://www.askorty.com/users/login", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=",
			"Mode=HTML", 
			LAST);

	lr_end_transaction("goToLoginPage", LR_AUTO);

	/************************************************************************************/


	/************************************* Log in ***************************************/

	lr_start_transaction("setAuthDataAndSignIn");	

	web_add_header ("X-CSRF-Token", "{csrfToken}");

	web_submit_data("login_2", 
					"Action=https://www.askorty.com/users/login", 
					"Method=POST", 
					"RecContentType=application/json", 
					"Referer=https://www.askorty.com/users/login",  
					"Mode=HTML", 
					"EncodeAtSign=YES", 
					ITEMDATA, 
					"Name=utf8", "Value=✓", ENDITEM, 
					"Name=user[login]", "Value=test_user_{timeStamp}@test.com", ENDITEM, 
					"Name=user[password]", "Value={timeStamp}", ENDITEM, 
					"Name=user[remember_me]", "Value=0", ENDITEM, 
					LAST);

    lr_end_transaction("setAuthDataAndSignIn", LR_AUTO);


	lr_start_transaction("goToHomePageAfterAuthorization");

	web_url("www.askorty.com", 
			"URL=https://www.askorty.com/", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/users/login", 
			"Mode=HTML", 
            LAST);

    lr_end_transaction("goToHomePageAfterAuthorization", LR_AUTO);

	/************************************************************************************/


	/************************* Go to embed card without answer **************************/


	lr_start_transaction("goToQuestionPage");

	web_reg_save_param("listIdQuestion", "LB=data-id='", "RB= data-type", "ORD=ALL", LAST);
	 
	web_url("question", 
			"URL=https://www.askorty.com/?type=question", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=",
			"Mode=HTML", 
			LAST);
	
	lr_end_transaction("goToQuestionPage", LR_AUTO);
	
	
	i = 0;
	
	do {
	
		lr_start_transaction("goToEmbedCardWithoutAnswer");
	
    	lr_save_string(lr_paramarr_random("listIdQuestion"),"idQuestion");
	
    	//lr_output_message("Embed Card id answer %s",lr_eval_string("{idQuestion}"));
	
		web_url("embedCardWithoutAnswer", 
				"URL=https://www.askorty.com/ecard/{idQuestion}", 
				"Resource=1", 
				"RecContentType=text/javascript", 
				"Referer=", 
				LAST);
	
		
		lr_end_transaction("goToEmbedCardWithoutAnswer", LR_AUTO);
	
	
		i = i + 1;
	
	} while ( i < 2 );


	/*************************************************************************************/


	/*********************** Go to embed card with answer ********************************/

	lr_start_transaction("prepareGoToEmbedCardWithAnswerPage");
	
		
	lr_start_transaction("goToAnswersPageForGetAllEmbedCardWithUrlAnswer");
	
	
	web_reg_save_param("listIdAnswer", "LB=ajax-modal-opener' data-href='/questions/", "RB=/embed_code", "ORD=ALL", LAST);
		
	web_url("answerpage", 
			"URL=https://www.askorty.com/?type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=", 
			"Snapshot=t1.inf", 
			"Mode=HTML", 
			LAST);
	
	web_url("answerpage", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/?type=answer", 
			"Snapshot=t2.inf", 
			"Mode=HTML", 
			LAST);
	
	web_url("www.askorty.com_3", 
			"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer=https://www.askorty.com/?type=answer", 
			"Snapshot=t3.inf", 
			"Mode=HTML", 
			LAST);
	
		
	lr_end_transaction("goToAnswersPageForGetAllEmbedCardWithUrlAnswer", LR_AUTO);
	
		
	i = 0;

	do {
	
		lr_start_transaction("goToEmbedCardWithAnswerPage");
	
    	lr_save_string(lr_paramarr_random("listIdAnswer"),"embedCode");
		//lr_output_message("Embed Card id card %s",lr_eval_string("{embedCode}"));

		web_url("EmbedCardWithAnswer", 
				"URL=https://www.askorty.com/ecard/{embedCode}", 
				"Resource=1", 
				"RecContentType=text/javascript", 
				"Referer=", 
				LAST);
	
        lr_end_transaction("goToEmbedCardWithAnswerPage", LR_AUTO);
	
    	i = i +1;
	
	}while ( i < 2);
	
		
	lr_end_transaction("prepareGoToEmbedCardWithAnswerPage", LR_AUTO);

	/************************************************************************************/


	/**************************** Ask Question to a celebrity ****************************/

	i = 0;

    do {
	

		lr_start_transaction("AskQuestionToCelebrity");
	
		lr_save_string(lr_paramarr_random("listUrlCelebrityDetailPages"),"celebrityDetailPage");
	
		web_url("CelebrityQuestionPage", 
				"URL=https://www.askorty.com/{celebrityDetailPage}?type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=", 
				"Mode=HTML", 
				LAST);
	
		web_reg_save_param("askByUserID", "LB=value=\"", "RB=\" type=\"hidden\" name=\"question[asked_by_user]\"", LAST);
		web_reg_save_param("askedToID", "LB=value=\"", "RB=\" name=\"question[asked_to][]\"", LAST);
	
		web_url("openWindowForQuestion", 
				"URL=https://www.askorty.com/{celebrityDetailPage}/questions/new?modal=true&msg=Success!%20You%20logged%20in.%20Please%20close%20this%20popup%20and%20click%20the%20'Ask%20me%20a%20question'%20button%20again", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/{celebrityDetailPage}", 
				"Mode=HTML", 
				LAST);
	
		//lr_output_message("askByUserID %s",lr_eval_string("{askByUserID}"));
		//lr_output_message("askedToID %s",lr_eval_string("{askedToID}"));
	
	
		web_submit_data("askQuestions", 
						"Action=https://www.askorty.com/{celebrityDetailPage}/questions", 
						"Method=POST", 
						"RecContentType=application/json", 
						"Referer=https://www.askorty.com/{celebrityDetailPage}?type=question", 
						"Mode=HTML", 
						ITEMDATA, 
						"Name=utf8", "Value=✓", ENDITEM, 
						"Name=question[asked_by_user]", "Value={askByUserID}", ENDITEM, 
						"Name=question[asked_to][]", "Value={askedToID}", ENDITEM, 
						"Name=question[text]", "Value=test_question {RandomQuestionText}?", ENDITEM, 
						LAST);
	
		lr_end_transaction("AskQuestionToCelebrity", LR_AUTO);

		i = i + 1;

	}while(i < 4);

	/************************************************************************************/


	/***************************** Post comment to an answer ****************************/
        
	i = 0;

    do {

		lr_start_transaction("postCommentToAnswer");

		web_reg_save_param("listAnswersOfCelebritys", "LB=data-href='/questions/", "RB=/comments?modal=true'", "ORD=ALL", LAST);

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
                "Mode=HTML", 
				LAST);

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				LAST);


		lr_save_string(lr_paramarr_random("listAnswersOfCelebritys"),"idAnswer");

		//lr_output_message("id answer %s",lr_eval_string("{idAnswer}"));


		web_url("comments", 
				"URL=https://www.askorty.com/questions/{idAnswer}/comments?modal=true", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				LAST);
	 
		web_submit_data("comments_2", 
						"Action=https://www.askorty.com/questions/{idAnswer}/comments", 
						"Method=POST", 
						"RecContentType=application/json", 
						"Referer=https://www.askorty.com/?type=answer", 
						"Mode=HTML", 
						ITEMDATA, 
						"Name=text", "Value=TestComment {timeStamp}", ENDITEM, 
						LAST);
	 
		lr_end_transaction("postCommentToAnswer", LR_AUTO);

		i = i + 1;

	}while(i < 4);

	/************************************************************************************/


	/*********************************** Voting for the question ************************************/

	i = 0;

	do {
		
		lr_start_transaction("voteForQuestion");

		web_reg_save_param("listOfQuestionsToCelebrities", "LB=data-id='", "RB=' data-loading-text='Voting", "ORD=ALL", LAST);

		web_url("www.askorty.com", 
				"URL=https://www.askorty.com/?type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=", 
				"Mode=HTML", 
				LAST);

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=question", 
				"Mode=HTML", 
				LAST);

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=question", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=question", 
				"Mode=HTML", 
				LAST);

		lr_save_string(lr_paramarr_random("listOfQuestionsToCelebrities"),"idQuestion");

		//lr_output_message("id question %s",lr_eval_string("{idQuestion}"));

		web_custom_request("utility", 
						   "URL=https://www.askorty.com/questions/{idQuestion}/utility?type=add_request", 
						   "Method=POST", 
						   "Resource=0", 
						   "RecContentType=application/json", 
						   "Referer=https://www.askorty.com/?type=question", 
						   "Mode=HTML", 
						   "EncType=", 
						   LAST);


		lr_end_transaction("voteForQuestion", LR_AUTO);

		i = i + 1;

	} while ( i < 2);

	/************************************************************************************/


	/********************************* Like an answer ***********************************/

	i = 0;

	do {

		lr_start_transaction("likeAnAnswer");

		web_reg_save_param("listAnswersOfCelebritys", "LB=data-href='/questions/", "RB=/comments?modal=true'", "ORD=ALL", LAST);

		web_url("www.askorty.com_2", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=2&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Snapshot=t2.inf", 
				"Mode=HTML", 
				LAST);

		web_url("www.askorty.com_3", 
				"URL=https://www.askorty.com/?only_cards=true&page_number=3&type=answer", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer=https://www.askorty.com/?type=answer", 
				"Mode=HTML", 
				LAST);


		lr_save_string(lr_paramarr_random("listAnswersOfCelebritys"),"idAnswer");

		//lr_output_message("id answer %s",lr_eval_string("{idAnswer}"));


		web_custom_request("utility", 
						   "URL=https://www.askorty.com/questions/{idAnswer}/utility?type=like", 
						   "Method=POST", 
						   "Resource=0", 
						   "RecContentType=application/json", 
						   "Referer=https://www.askorty.com/?type=answer", 
						   "Mode=HTML", 
						   "EncType=", 
						   LAST);
	 
		lr_end_transaction("likeAnAnswer", LR_AUTO);

		i = i + 1;

	} while ( i < 2 );

	/************************************************************************************/


	return 0;
}
